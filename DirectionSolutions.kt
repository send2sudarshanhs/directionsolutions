package com.example.newtest


fun main() {

    val upperX = 5
    val upperY = 5

    val initialAxis = 1 // input initial x axis
    val initialYAxis = 2 // input initial y axis
    val initialDirection = "N" // input direction of robot facing package

    val dirToMove = arrayOf("L", "M", "L", "M", "L", "M", "L", "M", "M") // inputs to move robot

    //test case 2
    /*  val initialAxis = 3 // input initial x axis
      val initialYAxis = 3 // input initial y axis
      val initialDirection = "E" // input direction of robot facing package
      val dirToMove = arrayOf("M","M","R","M","M","R","M","R","R","M") */

    val result = showMePath(upperX, upperY, initialAxis, initialYAxis, initialDirection, dirToMove)
    println(result)
}


private val eastDir = "E"
private val westDir = "W"
private val northDir = "N"
private val southDir = "S"
private val leftMov = "L"
private val rightMov = "R"
private val move = "M"

private fun showMePath(
    upperX: Int,
    upperY: Int,
    xAxis: Int,
    yAxis: Int,
    direction: String,
    dirArray: Array<String>
): String {


    var result = ""
    var currentDir = direction.uppercase()
    var currentXAxis = xAxis
    var currentYAis = yAxis
    var isValidInput = true

    if (upperX >= currentXAxis && upperY >= currentYAis) {


        if (currentDir == eastDir || currentDir == westDir || currentDir == southDir || currentDir == northDir) {

            dirArray.forEach loop@{ dir ->
                dir.uppercase()

                if (dir == leftMov || dir == rightMov || dir == move) {

                    when (dir) {

                        leftMov -> {
                            currentDir = when (currentDir) {
                                northDir -> westDir
                                southDir -> eastDir
                                westDir -> southDir
                                else -> northDir
                            }
                        }

                        rightMov -> {
                            currentDir = when (currentDir) {
                                northDir -> eastDir
                                southDir -> westDir
                                westDir -> northDir
                                else -> southDir
                            }
                        }

                        else -> {
                            if (currentDir == northDir) {
                                currentYAis += 1
                            }

                            if (currentDir == southDir) {
                                currentYAis -= 1
                            }

                            if (currentDir == eastDir) {
                                currentXAxis += 1
                            }

                            if (currentDir == westDir) {
                                currentXAxis -= 1
                            }
                        }
                    }
                } else {
                    isValidInput = false
                    result = "Given set of directions are wrong !"
                    return@loop
                }
            }
        } else {
            isValidInput = false
            result = "Given initial direction is wrong !"
        }

    } else {
        isValidInput = false
        result = "Direction should be with in limit !"
    }

    if (isValidInput) {
        result = "$currentXAxis $currentYAis $currentDir"
    }

    return result
}